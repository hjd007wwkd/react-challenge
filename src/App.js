import React, { Component } from 'react';
import fetchJsonp from "fetch-jsonp";
import Profile from './Profile'
import logo from './logo.svg';
import './App.css';
import './Profile.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      value: '',
      error: '',
      profile: {
        name: '',
        picture: '',
        email: '',
        phone: '',
        address: ''
      },
      cardDisplay: 'none',
      errorDisplay: 'none'
    };
  }

  handleChange = (e) => {
    const value = e.target.value
    this.setState(() => ({
      value
    }))
  }

  handleKeyPress = (e) => {
    //when press enter, still searching for MP office
    if(e.key === 'Enter') {
      this.handleClick();
    }
  }

  handleClick = () => {
    //get rid of space and keep the letter in uppercase
    const value = this.state.value.replace(/\s/g, '').toUpperCase();
    fetchJsonp('https://represent.opennorth.ca/postcodes/'+value+'/')
    .then((response) => {
      return response.json()
    }).then((json) => {
      //find whose office is MP
      json.representatives_centroid.forEach((item) => {
        if(item.elected_office === "MP") {
          this.setState(() => ({
            value: '',
            error: '',
            profile: {
              name: item.name,
              picture: item.photo_url,
              email: item.email,
              phone: item.offices[1].tel,
              address: item.offices[1].postal
            },
            cardDisplay: 'block',
            errorDisplay: 'none'
          }))
        }
      })
    }).catch((error) => {
      //error handling
      this.setState(() => ({
        error: 'You entered wrong postal code.',
        profile: {
          name: '',
          picture: '',
          email: '',
          phone: '',
          address: ''
        },
        cardDisplay: 'none',
        errorDisplay: 'block'
      }))
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">React Tech Challenge</h1>
        </header>

        <div className="error" style={{display: this.state.errorDisplay}}>
          <h1>{this.state.error}</h1>
        </div>

        <Profile display={this.state.cardDisplay} profile={this.state.profile}/>
        
        <p className="App-intro">
          Enter your postal code to find your MP!
          <br />
          <input type='text' value={this.state.value} onChange={this.handleChange} onKeyPress={this.handleKeyPress}/>
          <br />
          <button onClick={this.handleClick}>Go</button>
          {/* Feel free to add styles to App.css! */}
        </p>
      </div>
    );
  }
}

export default App;
