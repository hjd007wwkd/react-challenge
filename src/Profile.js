import React from 'react';

const Profile = ({profile, display}) => {
  return (
    <div style={{display}} className='profile'>
      <img src={profile.picture} alt='Nothing here!' className='pro-img'/>
      <p>{profile.name}</p>
      <p>{profile.email}</p>
      <p>{profile.phone}</p>
      <p>{profile.address}</p>
    </div>
  )
};

export default Profile;